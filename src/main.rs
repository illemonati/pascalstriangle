extern crate num_bigint;
extern crate num_iter;

use std::env::args;
use num_bigint::{BigUint, ToBigUint};
use num_iter::range_inclusive;

fn factorial(n: BigUint) -> BigUint {
    let mut ans: BigUint = BigUint::from(1u16);
    let mut range = range_inclusive(BigUint::from(1u16), n);

    for i in range {
        ans *= i;
    }

    ans
}

fn ncr(n: BigUint, r: BigUint) -> BigUint {
    factorial(n.clone())/(factorial(r.clone()) * factorial(n.clone() - r.clone()))
}

fn main() {
    let args: Vec<String> = args().collect();
    let row: &String = args.get(1).expect("1st argument should be row number!");
    let row: BigUint = row.parse::<BigUint>().expect("The row number could not be parsed!");
    println!();
    for n in range_inclusive(BigUint::from(0u16), row.clone()) {
        print!("{} ", ncr(row.clone(), n));
    }
    println!();
}
